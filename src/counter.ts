const COUNTER_CHANGED_EVENT = 'COUNTER_CHANGED_EVENT';

const counterChangedEvent = (value: number) => new CustomEvent("COUNTER_CHANGED_EVENT", {
  detail: {
    value,
  },
});

export function setupCounter(element: HTMLButtonElement) {
  let counter = 0;

  const setValue = (count: number) => {
    counter = count;
    element.dispatchEvent(counterChangedEvent(counter));
    render();
  }
  const render = () => element.innerHTML = `count is ${counter}`;

  element.addEventListener('click', () => setValue(counter + 1));

  document.addEventListener(COUNTER_CHANGED_EVENT, () => {
    element.innerHTML = `count is ${counter}`
  });

  render();
}
